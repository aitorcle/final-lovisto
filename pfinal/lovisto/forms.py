from django import forms
from django.contrib.auth.models import User
from .models import Aportation, Comment

class AportationForm(forms.ModelForm):

    class Meta:
        model = Aportation
        fields = ('title', 'url', 'description')

class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('username', 'email', 'password',)

class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = {'body'}
