
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

class User(models.Model):
    name = models.ForeignKey(User, on_delete=models.CASCADE)
    style = models.IntegerField(default=0)
    def __str__(self):
        return self.name.username

class Aportation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=64)
    url = models.URLField(max_length=200)
    description = models.TextField(default = "")
    date = models.DateTimeField()
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    ncomments = models.IntegerField(default=0)
    def __str__(self):
        return str(self.id)

class Aemet(models.Model):
    aportation = models.ForeignKey(Aportation, on_delete=models.CASCADE, default=None)
    city = models.CharField(max_length=64)
    province = models.CharField(max_length=64)
    copyright = models.TextField()

class Day(models.Model):
    day = models.CharField(max_length=64)
    temp_max = models.CharField(max_length=64)
    temp_min = models.CharField(max_length=64)
    sens_max = models.CharField(max_length=64)
    sens_min = models.CharField(max_length=64)
    hum_max = models.CharField(max_length=64)
    hum_min = models.CharField(max_length=64)
    aemet = models.ForeignKey(Aemet, on_delete=models.CASCADE, default=None)


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    aportation = models.ForeignKey(Aportation, on_delete=models.CASCADE)
    body = models.TextField()
    date = models.DateTimeField()

