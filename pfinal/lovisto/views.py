from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from urllib.parse import urlparse
from django.utils import timezone
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from .models import Aportation, User, Aemet, Day,  Comment
from .forms import AportationForm, UserForm, CommentForm
from .html_parser import html_parser
from .aemet_handler import AemetParser




@csrf_exempt
def analisis_sitio(url):
    parsed = urlparse(url)
    if (parsed.netloc == "www.aemet.es" or parsed.netloc == "aemet.es"):
        res = parsed.path.split('/')[-1]
        split = parsed.path.split('-')[0]
        if (split.startswith("/es/eltiempo/prediccion/municipios/")):
            municipio, id = res.split('-')
            id = id.split("id")[-1]
            if (municipio.isalpha() and id.isdigit()):
                return True

    elif (parsed.netloc == "www.youtube.com" or parsed.netloc == "youtube.com"):
        if (parsed.path == "/watch"):
            video = parsed.query.split("=")[0]
            if ("v" == video):
                return True

    elif (parsed.netloc == "es.wikipedia.org"):
        if (parsed.path.startswith("/wiki/")):
            return True

    return False


def index(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            user = User.objects.get(name=request.user)
            if "aport" in request.POST:
                aportation = Aportation(user=user)
                aportation.date = timezone.now()
                form = AportationForm(request.POST, instance=aportation)

                if form.is_valid():
                    f = form.save()
                    if (analisis_sitio(aportation.url)):
                        if ("aemet.es" in aportation.url):
                            if (not ("www" in aportation.url)):
                                url_org = aportation.url
                                split = aportation.url.split("//")
                                split.insert(1, "//www.")
                                aportation.url = ''.join(split)
                                aportation.save()
                                xml = html_parser(aportation.url)
                                AemetParser(xml)
                                aportation.url = url_org
                                aportation.save()
                            else:
                                xml = html_parser(aportation.url)
                                AemetParser(xml)



            elif "modo" in request.POST:
                if (user.style == 1):
                    user.modo = 0
                else:
                    user.modo = 1
                    user.save()
                return redirect("/")



        else:
            form = AportationForm()


    aemet_list = Aemet.objects.all()
    dia_list = Day.objects.all()

    if request.user.is_authenticated:
        user = User.objects.get(name=request.user)

    else:
        votos_list = None
        user = None

    context = { 'aemet_list': aemet_list, 'dia_list': dia_list,
                'perfil': user,
              }
    if request.user.is_authenticated:

        context2 = {'form': form, 'usuario': request.user}
        context.update(context2)
    return render(request, 'lovisto/inicio.html', context)

def logout_view(request):
    logout(request)
    return redirect("/")

def aportation(request, llave):
    if request.user.is_authenticated:
        if request.method == "POST":
            user = User.objects.get(usuario=request.user)
            if "comment" in request.POST:
                aport = Aportation.objects.get(id=llave)
                form = CommentForm(request.POST)

                if form.is_valid():
                    f = form.save(commit=False)
                    f.user = user
                    f.aportation = aport
                    f.date = timezone.now()
                    f.save()
                    aport.ncomments += 1
                    aport.save()

            elif "style" in request.POST:
                if (user.style == 1):
                    user.style = 0
                else:
                    user.style = 1
                user.save()
                return redirect("/" + str(llave))

            else:
                value = request.POST['submit']
                id = request.POST['aport_id']
                #Contar votos
                form = CommentForm()
        else:
            form = CommentForm()

    aportation = Aportation.objects.get(id=llave)
    try:
        aemet = Aemet.objects.get(aportation=aportation)
    except Aemet.DoesNotExist:
        aemet = None


    try:
        comments = Comment.objects.filter(aportation=aportation)
    except Comment.DoesNotExist:
        comments = None
    if request.user.is_authenticated:
        perf = User.objects.get(user=request.user)


    context = {'aportation': aportation, 'aemet': aemet, 'user': user,
               }

    if request.user.is_authenticated:
        context2 = {'form': form}
        context.update(context2)
    return render(request, 'lovisto/aportacion.html', context)
