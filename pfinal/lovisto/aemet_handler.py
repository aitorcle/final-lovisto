from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
from .models import Aemet, Day, Aportation

class AemetHandler(ContentHandler):
    """Clase para:
    Obtener contenido de un documento XML
    """

    def __init__(self):

        self.inRoot = False
        self.inContent = False
        self.inTemp = False
        self.inSens = False
        self.inHum = False

        self.content = ""
        self.url = ""
        self.city = ""
        self.province = ""
        self.day = []
        self.copyright = ""
        self.temp_max = []
        self.temp_min = []
        self.sens_max = []
        self.sens_min = []
        self.hum_max = []
        self.hum_min = []

    def startElement (self, name, attrs):
        if name == 'root':
            self.inRoot = True

        elif self.inTemp:
            if name == 'maxima':
                self.inContent = True
            if name == 'minima':
                self.inContent = True
        elif self.inSens:
            if name == 'maxima':
                self.inContent = True
            if name == 'minima':
                self.inContent = True
        elif self.inHum:
            if name == 'maxima':
                self.inContent = True
            if name == 'minima':
                self.inContent = True

        elif self.inRoot:
            if name == 'enlace':
                self.inContent = True
            elif name == 'nombre':
                self.inContent = True
            elif name == 'provincia':
                self.inContent = True
            elif name == 'temperatura':
                self.inTemp = True
            elif name == 'sens_termica':
                self.inSens = True
            elif name == 'humedad_relativa':
                self.inHum = True
            elif name == 'copyright':
                self.inContent = True
            elif name == 'dia':
                self.day.append(attrs.get('fecha'))

    def endElement (self, name):
        if name == 'root':
            self.inRoot = False
            try:
                aport = Aportation.objects.get(url=self.url)
            except Aportation.DoesNotExist:
                self.url = self.url.replace("https:", "http:")
                aport = Aportation.objects.get(url=self.url)

            A = Aemet(aportacion=aport, municipio=self.municipio, provincia=self.provincia, copyright=self.copyright)
            A.save()
            for x in range(7):
                D = Day(dia = self.dia[x], temp_max = self.temp_max[x], temp_min = self.temp_min[x], sens_max = self.sens_max[x],
                        sens_min = self.sens_min[x], hum_max = self.hum_max[x], hum_min = self.hum_min[x], aemet = A)
                D.save()

        elif self.inRoot:
            if name == 'enlace':
                self.url = self.content
                self.content = ""
                self.inContent = False
            elif name == 'nombre':
                self.city = self.content
                self.content = ""
                self.inContent = False
            elif name == 'provincia':
                self.province = self.content
                self.content = ""
                self.inContent = False
            elif name == 'temperatura':
                self.inTemp = False
            elif name == 'sens_termica':
                self.inSens = False
            elif name == 'humedad_relativa':
                self.inHum = False
            elif name == 'copyright':
                self.copyright = self.content
                self.content = ""
                self.inContent = False

            elif self.inTemp:
                if name == 'maxima':
                    self.temp_max.append(self.content)
                    self.content = ""
                    self.inContent = False
                if name == 'minima':
                    self.temp_min.append(self.content)
                    self.content = ""
                    self.inContent = False
            elif self.inSens:
                if name == 'maxima':
                    self.sens_max.append(self.content)
                    self.content = ""
                    self.inContent = False
                if name == 'minima':
                    self.sens_min.append(self.content)
                    self.content = ""
                    self.inContent = False
            elif self.inHum:
                if name == 'maxima':
                    self.hum_max.append(self.content)
                    self.content = ""
                    self.inContent = False
                if name == 'minima':
                    self.hum_min.append(self.content)
                    self.content = ""
                    self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class AemetParser:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = AemetHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)