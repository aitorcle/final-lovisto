# Generated by Django 3.1.7 on 2021-07-02 08:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lovisto', '0012_auto_20210702_0820'),
    ]

    operations = [
        migrations.CreateModel(
            name='Youtube',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tittle', models.CharField(max_length=64)),
                ('author', models.CharField(max_length=64)),
                ('video', models.CharField(max_length=500)),
                ('aportation', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='lovisto.aportation')),
            ],
        ),
    ]
